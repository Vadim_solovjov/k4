import java.util.*;

//https://stackoverflow.com/questions/343584/how-do-i-get-whole-and-fractional-parts-from-double-in-jsp-java
//https://www.javatpoint.com/java-integer-hashcode-method
//https://www.fatalerrors.org/a/java-design-of-rational-number-class.html
//https://introcs.cs.princeton.edu/java/92symbolic/Rational.java.html
//https://stackoverflow.com/questions/474535/best-way-to-represent-a-fraction-in-java
//https://codegym.cc/quests/lectures/questmultithreading.level01.lecture04
//https://stackoverflow.com/questions/2563608/check-whether-a-string-is-parsable-into-long-without-try-catch

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   private final long numerator;
   private final long denominator;

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction f1 = new Lfraction (3, 5);
      Lfraction f2 = new Lfraction (5, 7);
      Lfraction prd;
      prd = f1.times (f2);
   }
   

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
       if (b == 0) {
           throw new RuntimeException("Сannot be divided by 0");
       }
       long t = gcd(Math.abs(a), Math.abs(b));
       numerator = (b < 0 ? -a : a) / t;
       denominator = Math.abs(b) / t;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      if (getDenominator()==1) {
         return getNumerator()+"";
      }
      else{
         return getNumerator()+"/"+getDenominator();
      }
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      return compareTo((Lfraction)m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(getNumerator(), getDenominator());
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long a = getNumerator() * m.getDenominator() +
              getDenominator() * m.getNumerator();
      long b = getDenominator() * m.getDenominator();
      return new Lfraction(a, b);

      // getNumerator() -  первое левое верхнее число
      // getDenominator() -  первое  левое нижнее число
      // m.getNumerator() - второе правое верхнее число
      // m.getDenominator() - второе правое нижнее число

   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */



   public Lfraction times (Lfraction m) {
      long a = getNumerator() * m.getNumerator();
      long b = getDenominator() * m.getDenominator();
      long gcd = gcd(a, b);
      return new Lfraction((a / gcd),(b / gcd));
   }

   private static long gcd(long a, long b) {
      return b == 0 ? a : gcd(b, a % b);
   }


   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() { return new Lfraction(denominator, numerator);}

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() { return new Lfraction(-numerator, denominator);}

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus(Lfraction m) { return plus(m.opposite());}

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy(Lfraction m) { return times(m.inverse());}

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      if (minus(m).getNumerator() < 0){
         return -1;
      }
      else if (minus(m).getNumerator() == 0){
         return 0;
      }
      else {
         return 1;
      }
   }



   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone()
           throws CloneNotSupportedException {
      return new Lfraction(getNumerator(), getDenominator()); 
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return (int)toDouble();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      int part = (int) (getNumerator() / getDenominator());
      return new Lfraction(getNumerator() - (part * getDenominator()), getDenominator());
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return getNumerator() * 1.0 / getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(f*10*d)/10, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (s.isEmpty()) {
         throw new RuntimeException("Input is empty");
      }
      String[] numbers = s.split("/");
      if (numbers.length != 2){
         throw new RuntimeException("The entered word must contain exactly two numbers, the input contains: \"" + s + "\"");
      }
      if (isDigit(numbers[0])){ // isDigit проверяет является ли символ числом
         throw new RuntimeException("It is not a number: \"" + s + "\"" );
      }
      if(isDigit(numbers[1])){
         throw new RuntimeException("Denominator is not number: \"" + s + "\"" );
      }
      return new Lfraction(Long.parseLong(numbers[0]), Long.parseLong(numbers[1]));
   }

   private static boolean isDigit(String str) {  //votsin siit: https://stackoverflow.com/questions/2563608/check-whether-a-string-is-parsable-into-long-without-try-catch
      if (str == null) {
         return true;
      }
      int sz = str.length();
      for (int i = 0; i < sz; i++) {
         if (!Character.isDigit(str.charAt(i)) && str.charAt(i) != '-') {
            return true;
         }
      }
      return false;
   }

    public Lfraction pow(int n) {
        if (n < 0) {
           return inverse().pow(-n);
        } if (n == 0) {
           return new Lfraction(1, 1);
        } if (n % 2 == 0) {
           return times(this).pow(n >> 1);
        }
        return times(times(this).pow(n >> 1));
    }
}


